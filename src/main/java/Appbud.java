import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Marcin on 04.10.2017.
 */

public class Appbud {
    public static void main(String[] args){

        System.out.println("Hej, zaczynamy. Proszę podaj nazwę pliku do zczytania: ");
        Scanner scanner = new Scanner(System.in);
        String read = scanner.nextLine();

        Appbud obj = new Appbud();
        System.out.println("Poniżej została wyświetlona zawartość pliku, wybranego przez użytkownika: ");
        System.out.println(obj.getFileWithUtil(read));
        System.out.println("************************************************************************");
        System.out.println("Zawartość pliku po usunięciu wszystkich białych znaków: ");
        System.out.println(StringUtils.deleteWhitespace(obj.getFileWithUtil(read)));
    }
    private String getFileWithUtil(String fileName){
        System.out.println("");
        String resoult=""; // inicjalizacja

        ClassLoader classLoader = getClass().getClassLoader();
        try {
           resoult = IOUtils.toString(classLoader.getResourceAsStream(fileName),"UTF-8");

        } catch (IOException e){
            e.printStackTrace();
        }
        return resoult;
    }

}
